using System;
using SplashKitSDK;
using System.Collections.Generic;

public abstract class Entity {
    public Sprite Sprite;
    protected const float GRAVITY = (float) PlatformGame.GRAVITY;
    protected float SPEED = (float) PlatformGame.SPEED;
    public double MaxHealth = 100;
    public double Health;
    public PlatformGame Game { get; protected set; }
    protected float Y_LVL;
    public List<Weapon> Weapons { get; protected set; }
    public int WeaponIndex { get; protected set; }
    public Weapon Weapon { get; protected set; }
    public Direction Direction { get; protected set; }
    public String ID { get; protected set; }
    public List<Item> Items { get; protected set; }
    public bool Dummy { get; protected set; }

    public Entity(PlatformGame game) {
        Weapons = new List<Weapon>();
        WeaponIndex = 0;
        this.Game = game;
        ID = System.DateTime.Now.Ticks.ToString();
        Items = new List<Item>();
        Dummy = false;
        Health = MaxHealth;
    }

    public bool IsOnGround() {
        foreach (Sprite spr in Game.Blocks) {
            if ((Sprite.X + Sprite.Width > spr.X && Sprite.X < spr.X + spr.Width)  && (Sprite.Y >= spr.Y - Sprite.Height && Sprite.Y <= spr.Y )) {
                Y_LVL = spr.Y;
                return true;
            }
        } 

        Y_LVL = -1;
        return false;
    }

    public virtual void Draw() {
        for (int i = 0; i < Game.Bullets.Count; i++) {
            if (Game.Bullets[i].ID != ID) continue;
            Game.Bullets[i].Draw();
        }
    }

    public virtual void Update() {
        IsOnGround();
        
        Sprite.Dy += GRAVITY;

        Sprite.X += Sprite.Dx;

        if (Y_LVL != -1 && IsOnGround() && Sprite.Dy > 0) {
            Sprite.Y = Y_LVL - Sprite.Height;
            Sprite.Dy = 0;
        }
        else {
            Sprite.Y += Sprite.Dy;
        }

        Sprite.Update();

        Weapon.Attach();
    }

    public void SwitchWeapon(int index) {
        if (index > Weapons.Count || index < 0) return;
        Weapon = Weapons[index];
        WeaponIndex = index;
    }

    public void NextWeapon() {
        int nextIdx = WeaponIndex + 1;
        if (nextIdx >= Weapons.Count) SwitchWeapon(0);
        else SwitchWeapon(nextIdx);
    }

    public bool IsFull() {
        return Items.Count == 16;
    }

    public bool AddItem(Item item) {
        if (IsFull()) return false;
        Items.Add(item);
        return true;
    }

    public bool AddItem(Weapon item) {
        if (IsFull()) return false;
        
        foreach (Weapon w in Weapons) {
            if (w.Type != item.Type) continue;
            
            if (item.Ammo == item.MaxAmmo || w.Ammo == w.MaxAmmo) return w.AddClip();

            w.AddAmmo(item.Ammo);
            return true;
        }

        Items.Add(new Item(Game, item));
        Weapons.Add(item);

        return true;
    }

    public void Damage(int dmg) {
        if (Health - dmg < 0) Health = 0;
        else Health -= dmg;
    }

    public void SetDummy(bool val) {
        Dummy = val;
    }

    public void SetDirection(Direction dir) {
        Direction = dir;
    }
}