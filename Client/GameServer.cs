using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Linq;
using SplashKitSDK;

public enum ServerMessageType {
    BROADCAST,
    MOVE,
    WEAPON_FIRE,
    QUIT_GAME,
    UPDATE_PLAYER_NAME
}

public class GameServer {
    public static string Ip { get; private set; }
    public static int Port { get; private set; }
    //private static Connection socket;
    private static UdpClient udpClient;
    public static bool Enabled = false;

    public static void Setup(string ip, int port){
        Ip = ip;
        Port = port;
        Enabled = true;
        udpClient = new UdpClient(ip, port);
    }

    public static void Connect() {
        //socket = SplashKit.OpenConnection("server", Ip, (ushort) Port, ConnectionType.TCP);
        udpClient.Connect(new IPEndPoint(IPAddress.Parse(Ip), Port));
    }

    public static bool IsConnected() {
        // Enabled = socket.IsOpen;
        // return socket.IsOpen;
        return udpClient.Client.Connected;
    }

    public static void SendMessage(Json msg) {
        KeepAlive(); 

        try {
            String data = SplashKit.JsonToString(msg);
            udpClient.Send(Encoding.UTF8.GetBytes(data), Encoding.UTF8.GetBytes(data).Length);
            //socket.SendMessage(data);
            SplashKit.FreeJson(msg);
        }
        catch (Exception exc) {
            Console.WriteLine($"Server Error >> {exc.Message}");
        }
    }

    public static Json RecieveMessage() {
        KeepAlive(); 

        // if (socket.HasMessages) {
        //     return new Json(socket.ReadMessage().Data);
        // }

        try {
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(Ip), Port); 
            String data = Encoding.UTF8.GetString(udpClient.Receive(ref remoteEP));

            if (data == null) return new Json();

            return new Json(data);
        }
        catch {
            return new Json();
        }
    }

    private static void KeepAlive() {
        //if (!IsConnected()) socket.Reconnect();
    }

    public static void Stop() {
        //if (socket == null) return;
        Enabled = false;
        //socket.Close();
        if (IsConnected()) udpClient.Close();
    }

    public static Json GenerateJson(ServerMessageType type, Player player, String[] keys, String[] values) {
        Json json = new Json();
        json.AddString("id", player.ID);
        json.AddString("name", player.Name);
        json.AddString("type", type.ToString());
        for (int i = 0; i < keys.Length; i++) json.AddString(keys[i], values[i]);
        return json;
    }

    public static void SendPlayerMovement(Player player, float dx, float dy) {
        Json json = GenerateJson(ServerMessageType.MOVE, player, 
        new String[]{"x", "y", "dx", "dy", "angle", "weak_angle", "direction"}, 
        new String[]{player.Sprite.X.ToString(), player.Sprite.Y.ToString(), dx.ToString(), dy.ToString(), player.Weapon.Angle.ToString(), player.Weapon.WeakAngle.ToString(), player.Direction.ToString()}
        );
        SendMessage(json);
    }

    public static void SendWeaponFireAction(Player player, bool fire) {
        Json json = GenerateJson(ServerMessageType.WEAPON_FIRE, player, 
        new String[]{"angle", "weak_angle", "shoot"}, 
        new String[]{player.Weapon.Angle.ToString(), player.Weapon.WeakAngle.ToString(), fire.ToString()}
        );
        SendMessage(json);
    }
}

