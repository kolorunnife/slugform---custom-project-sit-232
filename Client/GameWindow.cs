using System;
using System.Linq;
using System.Collections.Generic;
using SplashKitSDK;

public class GameWindow {
    public static List<Button> Buttons = new List<Button>();
    public static List<InputField> InputFields = new List<InputField>();
    private static int PreviousFocusedIndex = -1;

    public struct Button
    {
        public Rectangle rect;
        public String text;
        public String name;
        public bool hover;
    }

    public struct InputField
    {
        public Rectangle rect;
        public String text;
        public String name;
        public bool focused;
        public int maxLength;
    }

    public static Button CreateButton(String name, int x, int y, int width, int height, String text) {
        Button btn = new Button();
        btn.rect = new Rectangle();
        btn.rect.X = x;
        btn.rect.Y = y;
        btn.rect.Width = width;
        btn.rect.Height = height;
        btn.hover = false;
        btn.text = text;
        btn.name = name;
        Buttons.Add(btn);
        return btn;
    }

    public static InputField CreateInputField(String name, int x, int y, int length) {
        InputField txt = new InputField();
        txt.rect = new Rectangle();
        txt.rect.X = x;
        txt.rect.Y = y;
        txt.rect.Width = 350;
        txt.rect.Height = 25;
        txt.text = "";
        txt.focused = false;
        txt.name = name;
        txt.maxLength = length;
        InputFields.Add(txt);
        return txt;
    }

    public static void SetupMenu(Window window) {
        int WIDTH = 350;
        //Play Game Button
        CreateButton("play", (window.Width - WIDTH) / 2, 50, WIDTH, 50, "Play Game");
        //Multiplayer Button
        CreateButton("multiplayer", (window.Width - WIDTH) / 2, 150, WIDTH, 50, "Multiplayer");
        //Preview Map
        CreateButton("map_preview", (window.Width - WIDTH) / 2, 250, WIDTH, 50, "Preview Map");
    }

    public static void SetupMultiplayerMenu(Window window) {
        CreateInputField("username", (window.Width - 350) / 2, 50, 10);
        CreateInputField("address", (window.Width - 350) / 2, 100, 25);
        CreateButton("server_connect", (window.Width - 350) / 2, 150, 350, 50, "Connect To Server");
        CreateButton("main_menu", (window.Width - 350) / 2, 225, 350, 50, "Back To Main Menu");
    }

    public static void DrawButton(Button button) {
        button.hover = SplashKit.PointInRectangle(SplashKit.MousePosition(), button.rect);
        Color clr = button.hover ? Color.DarkGreen : Color.Green;
        SplashKit.FillRectangle(clr, button.rect);
        SplashKit.DrawRectangle(Color.Black, button.rect);
        SplashKit.DrawText(button.text, Color.Black, (button.rect.X + (button.rect.Width / 2)) - (button.text.Length * 2.5), button.rect.Y + (button.rect.Height / 2));
    }

    public static void DrawInputField(InputField inputField) {
        Color clr = inputField.focused ? Color.Red : Color.Black;
        SplashKit.DrawRectangle(clr, inputField.rect);
        SplashKit.DrawText(inputField.text, Color.Black, inputField.rect.X + 2, inputField.rect.Y + 7);
    }

    public static void DrawButtons(Window window) {
        SplashKit.ClearScreen(Color.White);
        
        //Draw all buttons
        Buttons.ForEach(x => DrawButton(x));

        SplashKit.RefreshScreen(60);
    }

    public static void HandleButtons() {
        for (int i = 0; i < Buttons.Count; i++) {
            if (!SplashKit.PointInRectangle(SplashKit.MousePosition(), Buttons[i].rect)) continue;

            //User clicked play game button
            if (Buttons[i].name.Equals("play") && SplashKit.MouseClicked(MouseButton.LeftButton)) {
                Buttons.Clear();
                Program.SetState(WindowState.SINGLE_PLAYER_GAME);
            }
            //User clicked preview map button
            else if (Buttons[i].name.Equals("map_preview") && SplashKit.MouseClicked(MouseButton.LeftButton)) {
                Buttons.Clear();
                Program.SetState(WindowState.MAP_PREVIEW);
            }            
            //User clicked multiplayer button
            else if (Buttons[i].name.Equals("multiplayer") && SplashKit.MouseClicked(MouseButton.LeftButton)) {
                Buttons.Clear();
                Program.SetState(WindowState.MULTIPLAYER_MENU);
            }
            //User clicked back to main menu
            else if (Buttons[i].name.Equals("main_menu") && SplashKit.MouseClicked(MouseButton.LeftButton)) {
                Buttons.Clear();
                Program.SetState(WindowState.MAIN_MENU);
            }
            //User clicked connect to server button
            else if (Buttons[i].name.Equals("server_connect") && SplashKit.MouseClicked(MouseButton.LeftButton)) {
                String username = InputFields.Where(x => x.name.Equals("username")).First().text;
                if (username == String.Empty) {
                    Console.WriteLine("Input Username.");
                    return;
                }

                try {
                    String host = InputFields.Where(x => x.name.Equals("address")).First().text;
                    Console.WriteLine($"Connecting to {host}");
                    if (host.Contains(":")) GameServer.Setup(host.Split(":")[0], int.Parse(host.Split(":")[1])); 
                    else GameServer.Setup(host, 80);
                    GameServer.Connect();
                    //Ping Server
                    if (GameServer.IsConnected()) {
                        Console.WriteLine($"Server >> Connected to {host}");
                        Program.SetState(WindowState.MULTIPLAYER_GAME);
                    }
                }
                catch (Exception e) {
                    Console.WriteLine("Server >> " + e.Message);
                }
            }
        }
    }

    private static void SetText(int index, String text) {
        InputField input = InputFields[index];
        input.text = text;
        InputFields[index] = input;
    }

    private static void FocsusInputFields() {
        //Focusing fields
        for (int i = 0; i < InputFields.Count; i++) {
            if (!SplashKit.PointInRectangle(SplashKit.MousePosition(), InputFields[i].rect)) continue;
            
            if (SplashKit.MouseClicked(MouseButton.LeftButton)) {
                if (PreviousFocusedIndex == -1) PreviousFocusedIndex = i;

                //Unset previous focused input field
                if (PreviousFocusedIndex >= 0 && PreviousFocusedIndex < InputFields.Count) {
                    InputField prevField = InputFields[PreviousFocusedIndex];
                    prevField.focused = false;
                    InputFields[PreviousFocusedIndex] = prevField;
                }

                InputField field = InputFields[i];
                field.focused = true;
                InputFields[i] = field;
                PreviousFocusedIndex = i;
                break;
            }
        }
    }

    public static void HandleInputFields() {
        FocsusInputFields();

        for (int i = 0; i < InputFields.Count; i++) {
            if (!InputFields[i].focused) continue;

            String currentText = InputFields[i].text;

            KeyCode key = GetKeyPressed();
            bool CAPS = SplashKit.KeyDown(KeyCode.LeftShiftKey);

            if (key == KeyCode.UnknownKey) return;
            //Delete one character
            else if (key == KeyCode.BackspaceKey) currentText = currentText == String.Empty ? "" : currentText.Substring(0, currentText.Length - 1);
            //Add space
            else if (key == KeyCode.SpaceKey && currentText.Length < InputFields[i].maxLength) currentText += "#";
            //Add numbers
            else if (key.ToString().StartsWith("Num")  && currentText.Length < InputFields[i].maxLength) currentText += key.ToString().Replace("Num", "").Replace("Key", "");
            //Add Periods
            else if (key == KeyCode.PeriodKey  && currentText.Length < InputFields[i].maxLength) currentText += ".";
            //Add Colon
            else if (key == KeyCode.SemiColonKey  && currentText.Length < InputFields[i].maxLength) currentText += CAPS ? ":" : ";";
            //Add letters
            else if (key != KeyCode.LeftShiftKey  && currentText.Length < InputFields[i].maxLength) currentText += CAPS ? key.ToString().Replace("Key", "") : key.ToString().Replace("Key", "").ToLower();

            SetText(i, currentText);
            break;
        }
    }

    public static void DrawMultiplayerMenu(Window window) {
        SplashKit.ClearScreen(Color.White);

        //Draw Username Label
        window.DrawText("Username: ", Color.Black, InputFields[0].rect.X, InputFields[0].rect.Y - 10);
        //Draw Host Label
        SplashKit.DrawText("Host IP:", Color.Black, InputFields[1].rect.X, InputFields[1].rect.Y - 10);
        //Draw Inpt Fields
        InputFields.ForEach(x => DrawInputField(x));
        Buttons.ForEach(x => DrawButton(x));

        SplashKit.RefreshScreen(60);
    }

    private static List<KeyCode> AcceptedKeys() {
        List<KeyCode> keys = new List<KeyCode>();

        //Populate List with A-Z
        for (int i = 65; i < 65 + 26; i++) {
            Enum.TryParse(((char) i) + "Key", out KeyCode key);
            keys.Add(key);
        }

        //Populate List with 0-9
        for (int i = 0; i < 10; i++) {
            Enum.TryParse($"Num{i}Key", out KeyCode key);
            keys.Add(key);
        }

        keys.Add(KeyCode.SpaceKey);
        keys.Add(KeyCode.BackspaceKey);
        keys.Add(KeyCode.PeriodKey);
        keys.Add(KeyCode.LeftShiftKey);
        keys.Add(KeyCode.SemiColonKey);

        return keys;
    }

    public static KeyCode GetKeyPressed() {
        foreach (KeyCode key in AcceptedKeys()) {
            if (SplashKit.KeyTyped(key)) return key;
        }
        return KeyCode.UnknownKey;
    }
}