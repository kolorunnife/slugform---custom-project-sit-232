using System;
using SplashKitSDK;

public enum ItemType {
    GOLD_COIN,
    WEAPON,
    HP_BOTTLE
}

public class Item {
    public ItemType Type { get; private set; }
    public Weapon Weapon { get; private set; }
    public Bitmap Img { get; private set; }
    public Sprite Sprite { get; private set; }
    private float Y_LVL;
    private PlatformGame game;

    public Item(PlatformGame game, ItemType type) {
        this.Type = type;
        this.game = game;
        Img = SplashKit.BitmapNamed(type.ToString().ToLower());
        Sprite = new Sprite(Img);
    }

    public Item(PlatformGame game, Weapon weapon) {
        Type = ItemType.WEAPON;
        this.Weapon = weapon;
        this.game = game;
        Img = weapon.image;
        Sprite = new Sprite(Img);
    }

    public void Draw() {
        Sprite.Draw();
    }

    public bool IsOnGround() {
        foreach (Sprite spr in game.Blocks) {
            if ((Sprite.X + Sprite.Width > spr.X && Sprite.X < spr.X + spr.Width)  && (Sprite.Y >= spr.Y - Sprite.Height && Sprite.Y <= spr.Y )) {
                Y_LVL = spr.Y;
                return true;
            }
        } 

        Y_LVL = -1;
        return false;
    }

    public void Update() {
        IsOnGround();

        Sprite.Dy += (float) PlatformGame.GRAVITY;

        if (Y_LVL != -1 && IsOnGround() && Sprite.Dy > 0) {
            Sprite.Y = Y_LVL - Sprite.Height;
            Sprite.Dy = 0;
        }
        else {
            Sprite.Y += Sprite.Dy;
        }

        Sprite.Update();
    }
    
}