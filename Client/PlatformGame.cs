using System;
using System.Collections.Generic;
using SplashKitSDK;
using System.Linq;

public class PlatformGame {
    public const double GRAVITY = 0.2;
    public const double SPEED = 3;
    public Window window { get; private set; }
    public List<Bullet> Bullets { get; private set; }
    public List<Enemy> Enemies { get; private set; }
    public List<Player> Players { get; private set; }
    public List<Item> Items { get; private set; }
    public List<Sprite> Blocks = new List<Sprite>();
    public List<GameEffect> Effects = new List<GameEffect>();
    private long prevItemDropDelay = 0;

    public PlatformGame(Window window) {
        Bullets = new List<Bullet>();
        Enemies = new List<Enemy>();
        Players = new List<Player>();
        Items = new List<Item>();

        Players.Add(new Player(this));
        this.window = window;
    }

    public void LoadAssests() {
        SplashKit.LoadResourceBundle("bundle", "assets.txt"); 
        SplashKit.BitmapSetCellDetails(SplashKit.BitmapNamed("enemy_set"), 28, 38, 4, 2, 8);
        SplashKit.BitmapSetCellDetails(SplashKit.BitmapNamed("player_set"), 26, 32, 6, 2, 12);
        SplashKit.BitmapSetCellDetails(SplashKit.BitmapNamed("bullet_explode_set"), 32, 32, 8, 1, 8);
        SplashKit.BitmapSetCellDetails(SplashKit.BitmapNamed("enemy_explode_set"), 64, 64, 8, 1, 8);
    }

    public void SetupEntites() {
        Players[0].Setup();
        ParseMap(null);
    }

    public void DrawHUD() {
        Color hud_clr = Color.RGBAColor(49, 47, 49, 200);

        //Draw Player Icon Box Area
        Bitmap img = SplashKit.BitmapNamed("player_icon");
        double start_y = (32 - img.Height) / 2;

        //Draw Player Icon
        window.FillRectangle(hud_clr, 0, 0, 32, 50);
        window.DrawBitmap(img,  (32 - img.Width) / 2, start_y);

        //Draw Player Hp
        window.FillRectangle(Color.Green, (32 - img.Width) / 2, 35, (Players[0].Health / 100) * (32 - (32 - img.Width)), 10);
        window.DrawRectangle(Color.DarkOliveGreen, (32 - img.Width) / 2, 35, 32 - (32 - img.Width), 10);
        window.DrawText($"{Players[0].Health}", Color.White, "hud_font", 10, (32 - img.Width) / 2, 35);

        //Draw Player Inventory Box Area
        window.FillRectangle(hud_clr, 32, 0, window.Width * 0.85, 40 + start_y);
        window.DrawRectangle(Color.Gray, 35, start_y, window.Width * 0.50, 32);

        //Draw Inventory Items
        for (int i = 0; i < (window.Width * 0.50) / 40; i++) {
            int startX = 35 + 40 * i;
            window.DrawLine(Color.Gray, startX, start_y, startX, start_y + 31);
            
            if (i < Players[0].Items.Count) {
                img = Players[0].Items[i].Img;
                window.DrawBitmap(img, startX + (40 - img.Width) / 2, start_y + (40 - img.Height) / 2.5);
            }
        }

        //Draw Score
        img = SplashKit.BitmapNamed("gold_coin");
        window.DrawBitmap(img, 32 + window.Width * 0.52, start_y + img.Height / 3.5);
        window.DrawText(Players[0].Score.ToString(), Color.White, "hud_font", 16, 32 + window.Width * 0.54, start_y + img.Height / 2.5);

        //Draw Gun Ammo Status w/ Text
        double ammo_percent = (double) Players[0].Weapon.Ammo / (double) Players[0].Weapon.MaxAmmo;
        window.FillRectangle(hud_clr, 32 + window.Width * 0.85, 0, window.Width * 0.15, 60);
        window.FillRectangle(Color.Green, 32 + window.Width * 0.86, start_y, ammo_percent * (window.Width * 0.135 - 32), 10);

        String ammoTextOverlay;
        if (Players[0].Weapon.Ammo != -1) ammoTextOverlay = Players[0].Weapon.Ammo + "/" + Players[0].Weapon.MaxAmmo;
        else ammoTextOverlay = "Infinite";
       
        window.DrawText(ammoTextOverlay, Color.White, (32 + window.Width * 0.86) + (window.Width * 0.135 - 32) / 2 - ammoTextOverlay.Length, 6);
        window.DrawRectangle(Color.DarkOliveGreen, 32 + window.Width * 0.86, start_y, window.Width * 0.135 - 32, 10);

        //Draw Current Weapon
        window.DrawBitmap(Players[0].Weapon.image, 32 + window.Width * 0.90, 30, SplashKit.OptionScaleBmp(2, 2));

        //Draw Ammo Clips
        ammoTextOverlay = $"{Players[0].Weapon.Ammo}/{Players[0].Weapon.Clips * (Players[0].Weapon.MaxAmmo < 0 ? 1: Players[0].Weapon.MaxAmmo)}";
        window.DrawText(ammoTextOverlay, Color.White, window.Width - ammoTextOverlay.Length * 10.5, 47);
    }

    public void GenerateMapFile() {
        if (System.IO.File.Exists(SplashKit.PathToResources() + "/map.txt")) { return; }

        //Generating Map Data
        Bitmap block = SplashKit.BitmapNamed("block");
        String rowBlocks = "";
        String[] Blocks = new String[(int) (window.Height / (block.Height * 0.75)) + 1];

        //Generating width of level
        for (int i = 0; i < (window.Width / block.Width) + 1; i++) rowBlocks += "0,";
        rowBlocks = rowBlocks.Substring(0, rowBlocks.Length - 1);

        //Generating the height of the level
        for (int i = 0; i < (int) (window.Height / (block.Height * 0.75)); i++) Blocks[i] = rowBlocks;

        //Print data to file
        System.IO.File.WriteAllLines(SplashKit.PathToResources() + "/map.txt", Blocks);
    }

    public void ParseMap(String[] data) {
        Bitmap block = SplashKit.BitmapNamed("block");
        Sprite sprBlock;

        String[] mapData = data == null ? System.IO.File.ReadAllLines(SplashKit.PathToResources() + "/map.txt"): data;

        Blocks.Clear();

        for (int i = 0; i < mapData.Length; i++) {
            String row = mapData[i];
            for (int j = 0; j < row.Split(",").Length; j++) {
                if (row.Split(",")[j] == "1") {
                    sprBlock = new Sprite(block);
                    sprBlock.X = j * block.Width;
                    sprBlock.Y = i * (block.Height * 0.75f);
                    Blocks.Add(sprBlock);
                }
            } 
        }
    }

        public void PreviewDraw() {
        window.Clear(Color.White);
        
        //Draw Background Dynamically
        Bitmap background = SplashKit.BitmapNamed("background");
        for (int i = 0; i < (window.Width / background.Height) + 1; i++) {
            for (int j = 0; j < (window.Width / background.Width) + 1; j++) {
                SplashKit.DrawBitmap(background, j * background.Width, i * background.Height);
            }
        }

        for (int i = 0; i < Players.Count; i++) {
            if (Players[i].Health <= 0) continue;
            Players[i].Draw();
        }

        //Draw Blocks Dynamically
        for (int i = 0; i < Blocks.Count; i++) Blocks[i].Draw();

        //Draw Cursor
        window.DrawBitmap(SplashKit.BitmapNamed("cursor"), MousePosition().X, MousePosition().Y);

        SplashKit.RefreshScreen(60);
    }

    public void Draw() {
        window.Clear(Color.White);
        
        //Draw Background Dynamically
        Bitmap background = SplashKit.BitmapNamed("background");
        for (int i = 0; i < (window.Width / background.Height) + 1; i++) {
            for (int j = 0; j < (window.Width / background.Width) + 1; j++) {
                SplashKit.DrawBitmap(background, j * background.Width, i * background.Height);
            }
        }

        for (int i = 0; i < Enemies.Count; i++) Enemies[i].Draw();

        for (int i = 0; i < Items.Count; i++)  Items[i].Draw();

        for (int i = 0; i < Players.Count; i++) {
            if (Players[i].Health <= 0) continue;
            Players[i].Draw();
        }

        //Draw Blocks Dynamically
        for (int i = 0; i < Blocks.Count; i++) Blocks[i].Draw();

        //Draw Explosion Effects
        for (int i = 0; i < Effects.Count; i++) {
            Effects[i].Draw();
        }

        //Draw Cursor
        window.DrawBitmap(SplashKit.BitmapNamed("cursor"), MousePosition().X, MousePosition().Y);

        DrawHUD();

        SplashKit.RefreshScreen(60);
    }

    public void HandleInput() {
        Players[0].HandleInput();
    }

    public void Update() {
        for (int i = 0; i < Enemies.Count; i++) {

            //Drop Items on enemy kill
            if (Enemies[i].Health <= 0) {

                Item item = new Item(this, ItemType.GOLD_COIN);
                item.Sprite.Position = Enemies[i].Sprite.Position;
                Items.Add(item);

                item = new Item(this, Enemies[i].Weapon);
                item.Sprite.Position = Enemies[i].Sprite.Position;
                Items.Add(item);

                Point2D effectPos = Enemies[i].Sprite.Position;
                effectPos.Y -= Enemies[i].Sprite.Height;
                Effects.Add(new GameEffect(GameEffectType.ENEMY_EXPLODE, effectPos));

                Enemies.Remove(Enemies[i]);
                continue;
            }

            Enemies[i].Update();
            Enemies[i].Shoot();
        }

        for (int i = 0; i < Players.Count; i++) {
            if (Players[i].Health <= 0) continue;
            Players[i].Update();
        }

        for (int i = 0; i < Items.Count; i++)  Items[i].Update();

        for (int i = 0; i < Bullets.Count; i++) {
            Bullets[i].Update();

            if (Bullets[i].IsOffScreen()) Bullets.Remove(Bullets[i]);
        }

        //Update Explosion Effects
        for (int i = 0; i < Effects.Count; i++) {
            if (Effects[i].Ended()) {
                Effects.RemoveAt(i);
                continue;
            }

            Effects[i].Update();
        }

        CheckCollisions();
    }

    public static Point2D MousePosition() {
        Window win = SplashKit.WindowNamed("Game");
        return new Point2D{X = SplashKit.MouseX(), Y = SplashKit.MouseY()};
    }

    public void CheckCollisions() {
        for (int j = 0; j < Players.Count; j++) {
            //Skip if player is dead
            if (Players[j].Health <= 0) continue;

            for (int i = 0; i < Bullets.Count; i++) {

                if (SplashKit.SpriteCollision(Players[j].Sprite, Bullets[i].Sprite) && Bullets[i].ID != Players[j].ID) {
                    //Draw damage indicator effect
                    Effects.Add(new GameEffect(Color.White, Bullets[i].Damage.ToString(), new Point2D{X = Bullets[i].Sprite.Position.X, Y = Players[j].Sprite.Position.Y - 10}, 0.05));
                    Players[j].Damage(Bullets[i].Damage);
                    Bullets.Remove(Bullets[i]);
                }

            }

            //Check Item Collisions with Players
            for (int i = 0; i < Items.Count; i++) {

                if (SplashKit.SpriteCollision(Players[j].Sprite, Items[i].Sprite)) {
                    if (Items[i].Type == ItemType.GOLD_COIN) {
                        SplashKit.PlaySoundEffect("pickup_coin");
                        Players[j].Score += 1;
                        Items.Remove(Items[i]);
                    }
                    else if (Items[i].Type == ItemType.WEAPON) {
                        if (Players[j].AddItem(Weapon.ConvertWeapon(Players[j], Items[i].Weapon))) {
                            Items.Remove(Items[i]);
                        }
                    } 
                    else if (Items[i].Type == ItemType.HP_BOTTLE) {
                        if (Players[j].Health != 100) {
                            Players[j].Health = Players[j].MaxHealth;
                            Items.Remove(Items[i]);
                        }
                    } 
                }

            }
        }

        //Check for collisions between enemies and bullets
        for (int j = 0; j < Enemies.Count; j++) {
            for (int i = 0; i < Bullets.Count; i++) {
                if (SplashKit.SpriteCollision(Enemies[j].Sprite, Bullets[i].Sprite) && Bullets[i].ID != Enemies[j].ID) {
                    Enemies[j].Damage(Bullets[i].Damage);
                    //Draw damagae indicator effect
                    Effects.Add(new GameEffect(Color.White, Bullets[i].Damage.ToString(), new Point2D{X = Bullets[i].Sprite.Position.X, Y = Enemies[j].Sprite.Position.Y - 10}, 0.05));
                    Bullets.Remove(Bullets[i]);
                }
            }
        }

        //Add effects for bullet collisions
        for (int j = 0; j < Blocks.Count; j++) {
            for (int i = 0; i < Bullets.Count; i++) {
                if (SplashKit.SpriteCollision(Blocks[j], Bullets[i].Sprite)) {
                    Effects.Add(new GameEffect(GameEffectType.BULLET_EXPLODE, Bullets[i].Sprite.Position));
                    Bullets.Remove(Bullets[i]);
                }
            }
        }
    }

    public void SpawnEnemies() {
        if (Enemies.Count == 0) {
            for (int i = 0; i < SplashKit.Rnd(10); i++) {
                Enemy enemy = new Enemy(this);
                enemy.Setup();
                enemy.setTarget(0);
                Enemies.Add(enemy);
            }
        }
    }

    public void DropItems() {
        long time = System.DateTime.Now.Ticks / 10000000;
        int itemDropDelay = 15;

        if (time - prevItemDropDelay > itemDropDelay) {
            Item item = new Item(this, ItemType.HP_BOTTLE);

            //Drop only one HP bottle
            if (Items.Where(x => x.Type == ItemType.HP_BOTTLE).LongCount() < Players.Count + 1) {
                Items.Add(item);
                item.Sprite.X = SplashKit.Rnd(window.Width);
                prevItemDropDelay = time;
            }
            
        }
    }

    private void CheckPlayer(String id, String name) {
        if (Players.Exists(x => x.ID == id)) return;

        Player player = new Player(this);
        player.Name = name;
        player.ChangeID(id);
        player.Setup();
        player.SetDummy(true);
        Players.Add(player);

        Console.WriteLine($"{player.ID} has been added. Player Count: {Players.Count}");
    }

    public void HandleIncomingMessages() {
        while (GameServer.Enabled) {
            SplashKit.CheckNetworkActivity();

            Json json = GameServer.RecieveMessage();
            if (json == null) continue;

            Console.WriteLine(SplashKit.JsonToString(json));

            String id = json.ReadString("id");
            Enum.TryParse(json.ReadString("type"), out ServerMessageType type);
            CheckPlayer(id, json.ReadString("name"));

            //Broadcast Message
            if (type == ServerMessageType.BROADCAST) {
                Effects.Add(new GameEffect(Color.White, json.ReadString("broadcast"), new Point2D{ X = 50, Y = 150}, 10));
            }
            //Player Movement Update
            else if (type == ServerMessageType.MOVE) {
                float dx = float.Parse(json.ReadString("dx"));
                float dy = float.Parse(json.ReadString("dy"));
                float x = float.Parse(json.ReadString("x"));
                float y = float.Parse(json.ReadString("y"));
                float angle = float.Parse(json.ReadString("angle"));
                float weakAngle = float.Parse(json.ReadString("weak_angle"));
                Enum.TryParse(json.ReadString("direction"), out Direction dir);

                Player player = Players.Where(pl => pl.ID == id).First();

                if (player.Sprite.X  < x - 1 || player.Sprite.X  > x + 1) player.Sprite.X = x + dx;
                if (player.Sprite.Y  < y - 1 || player.Sprite.Y  > y + 1) player.Sprite.Y = y + dy;

                player.Sprite.Dx = dx;
                player.Sprite.Dy = dy;
                player.SetDirection(dir);
                player.Weapon.SetAngles(angle, weakAngle);
            }
            //Fire Weapon
            else if (type == ServerMessageType.WEAPON_FIRE) {
                Player player = Players.Where(pl => pl.ID == id).First();
                float angle = float.Parse(json.ReadString("angle"));
                float weakAngle = float.Parse(json.ReadString("weak_angle"));

                player.Weapon.SetAngles(angle, weakAngle);
                if (json.ReadString("shoot").Equals("True")) player.Weapon.Shoot();
            }
            //Player Quit
            else if (type == ServerMessageType.QUIT_GAME) {
                if (id == Players[0].ID) continue;
                Players.Remove(Players.Where(x => x.ID == id).First());
            }

            if (json != null) SplashKit.FreeJson(json);
        }
    }
}

public enum GameEffectType {
    BULLET_EXPLODE,
    ENEMY_EXPLODE,
    TEMP_TEXT
}

public class GameEffectText {
    public String text = null;
    public double delay = 0;
    public Color color = Color.White;
    public Point2D pos = new Point2D{X = 0, Y = 0};

    public GameEffectText() { }
}


public class GameEffect {
    private Sprite spr;
    public GameEffectType type { get; private set; }
    private GameEffectText effectText = new GameEffectText();
    private long prevTime = 0;

    public GameEffect(GameEffectType type, Point2D pos) {
        switch (type) {
            case GameEffectType.BULLET_EXPLODE:
                spr = new Sprite(SplashKit.BitmapNamed("bullet_explode_set"), SplashKit.AnimationScriptNamed("bullet_explode_set"));
                spr.StartAnimation("explode");
            break;

            case GameEffectType.ENEMY_EXPLODE:
                SplashKit.PlaySoundEffect("robot_explode");
                spr = new Sprite(SplashKit.BitmapNamed("enemy_explode_set"), SplashKit.AnimationScriptNamed("enemy_explode_set"));
                spr.StartAnimation("explode");
            break;
        }

        this.type = type;
        spr.Position = pos;
    }

    public GameEffect(Color cl, String txt, Point2D pos, double delay) {
        this.type = GameEffectType.TEMP_TEXT;
        effectText.text = txt;
        effectText.color = cl;
        effectText.pos = pos;
        effectText.delay = delay;
        prevTime = System.DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
    }

    public void Draw() {
        if (type == GameEffectType.TEMP_TEXT) {
            SplashKit.DrawText(effectText.text, effectText.color, effectText.pos.X, effectText.pos.Y);
            return;
        }

        spr.Draw();
    }

    public void Update() {
        if (type == GameEffectType.TEMP_TEXT) return;

        spr.Update();
        spr.UpdateAnimation();
    }

    public bool Ended() {
        if (type == GameEffectType.TEMP_TEXT) {
            long time = System.DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            if ((double)(time - prevTime) / 1000f > effectText.delay) {
                prevTime = time;
                return true;
            }

            return false;
        }

        return spr.AnimationHasEnded;
    }
}