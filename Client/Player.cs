using System;
using SplashKitSDK;
using System.Linq;

public enum Direction {
    RIGHT,
    LEFT
}

public class Player : Entity {

    public int Score = 0;
    public String Name;
    private String PrevKeyboardAction = "";
    private Point2D PrevPos = new Point2D{X=0,Y=0};

    public Player(PlatformGame game) : base(game) {  }

    public void Setup() {
        Sprite = new Sprite(SplashKit.BitmapNamed("player_set"), SplashKit.AnimationScriptNamed("player_set"));
        Sprite.StartAnimation("walk_right");

        Score = 0;
        Health = MaxHealth;
        Weapons.Clear();
        
        PlayerWeapon weapon = new PlayerWeapon(this, WeaponType.MINIGUN);
        AddItem(weapon);
        SwitchWeapon(0);
    }

    public void ChangeID(String id) {
        ID = id;
    }

    public void HandleInput() {
        Sprite.Dx = 0;

        if (Health <= 0) return;

        //Back to Main Menu
        if (SplashKit.KeyTyped(KeyCode.EscapeKey)) Program.SetState(WindowState.MAIN_MENU);

        if (SplashKit.KeyTyped(KeyCode.RKey) && Program.State == WindowState.MAP_PREVIEW) Game.SetupEntites();

        //Go Right
        if (SplashKit.KeyDown(KeyCode.DKey) && Sprite.X < (Game.window.Width - Sprite.Width)) {
            Sprite.Dx = SPEED;
            Direction = Direction.RIGHT;
            if (!Sprite.AnimationName().Equals("walk_right")) Sprite.StartAnimation("walk_right");
        }

        //Go Left
        if (SplashKit.KeyDown(KeyCode.AKey) && Sprite.X > 0) {
            Sprite.Dx = -SPEED;
            Direction = Direction.LEFT;
            if (!Sprite.AnimationName().Equals("walk_left")) Sprite.StartAnimation("walk_left");
        }

        //Jump
        if ((SplashKit.KeyTyped(KeyCode.SpaceKey) || SplashKit.KeyTyped(KeyCode.WKey)) && IsOnGround()) {
            Sprite.Dy = -SPEED * 1.4f;
        }

        //Shoot
        if ((SplashKit.MouseClicked(MouseButton.LeftButton) || SplashKit.MouseDown(MouseButton.LeftButton)) && Weapon.HasAmmo()) {
            Weapon.Shoot();
        }

        //Switch Weapon
        if (SplashKit.KeyTyped(KeyCode.TabKey) || SplashKit.MouseWheelScroll().Y > 0) {
            NextWeapon();
        }

        //Reload Weapon
        if (SplashKit.KeyTyped(KeyCode.RKey) || SplashKit.MouseClicked(MouseButton.RightButton)) {
            Weapon.Reload();
        }
    }

    public void NetworkHandleInput() {
        //Prevent player from glitching and pressing two keys at once
        if (SplashKit.KeyDown(KeyCode.DKey) && SplashKit.KeyDown(KeyCode.AKey)) return;

        //KeyDown Send Move Data
        if (SplashKit.KeyDown(KeyCode.DKey) && !PrevKeyboardAction.Equals("right")) {
            Direction = Direction.RIGHT;
            GameServer.SendPlayerMovement(this, SPEED, Sprite.Dy);
            PrevKeyboardAction = "right";
        }
        else if (SplashKit.KeyDown(KeyCode.AKey) && !PrevKeyboardAction.Equals("left")) {
            Direction = Direction.LEFT;
            GameServer.SendPlayerMovement(this, -SPEED, Sprite.Dy);
            PrevKeyboardAction = "left";
        }
        //Jump
        else if ((SplashKit.KeyDown(KeyCode.SpaceKey) || SplashKit.KeyDown(KeyCode.WKey)) && IsOnGround()) {
            GameServer.SendPlayerMovement(this, Sprite.Dx, -SPEED * 1.4f);
        }
        //Shoot
        if ((SplashKit.MouseClicked(MouseButton.LeftButton)) && Weapon.HasAmmo()) {
            GameServer.SendWeaponFireAction(this, true);
        }

        //KeyUp Send Stop Move Data
        if (SplashKit.KeyReleased(KeyCode.DKey) || SplashKit.KeyReleased(KeyCode.AKey)) {
            GameServer.SendPlayerMovement(this, 0, Sprite.Dy);
            PrevKeyboardAction = "idle";
        }

        //Mouse Movement
        // Vector2D mouseMovement = SplashKit.MouseMovement();
        // int thrs = 1;
        // if ( (mouseMovement.X >= -thrs && mouseMovement.X <= thrs && mouseMovement.Y >= -thrs && mouseMovement.Y <= thrs) && PrevMouseMoveAction == "moving"){
        //     GameServer.SendWeaponFireAction(this, false);
        //     PrevMouseMoveAction = "";
        // }
        // if ( ( (mouseMovement.X > thrs || mouseMovement.X < -thrs) && (mouseMovement.Y > thrs || mouseMovement.Y < thrs) )){
        //     PrevMouseMoveAction = "moving";
        // }

        Console.WriteLine("#");
    }

    public override void Update() {
        base.Update();

        if (Sprite.Dx == 0 && Direction == Direction.RIGHT && !Sprite.AnimationName().Equals("idle_right")) Sprite.StartAnimation("idle_right");
        else if (Sprite.Dx == 0 && Direction == Direction.LEFT && !Sprite.AnimationName().Equals("idle_left")) Sprite.StartAnimation("idle_left");
        else if (Sprite.Dx > 0 && !Sprite.AnimationName().Equals("walk_right")) Sprite.StartAnimation("walk_right");
        else if (Sprite.Dx < 0 && !Sprite.AnimationName().Equals("walk_left")) Sprite.StartAnimation("walk_left");
        else if (Sprite.Dx != 0 && Sprite.AnimationHasEnded) Sprite.ReplayAnimation();

        Sprite.UpdateAnimation();
    }

    public override void Draw() {
        Sprite.Draw();
        base.Draw();
        Weapon.Draw(Direction);

        if (Program.State == WindowState.MULTIPLAYER_GAME) {
            SplashKit.DrawText(Name, Color.BrightGreen, Sprite.X - Name.Length / 2, Sprite.Y - 16);
        }
    }
}

public class Enemy : Entity {
    private int targetIndex = 0;
    private int purseDistance = 300;

    public Enemy(PlatformGame game) : base(game) { 
        MaxHealth = 10;
        Health = 10;
    }

    public void setTarget(int index) {
        if (index < 0 || index > Game.Players.Count) return;

        targetIndex = index;
    }

    public void Setup() {
        Sprite = new Sprite(SplashKit.BitmapNamed("enemy_set"), SplashKit.AnimationScriptNamed("enemy_set"));
        Sprite.StartAnimation("ground_right");

        //Get Random Weapon Type
        WeaponType randomType = (WeaponType) SplashKit.Rnd(Enum.GetNames(typeof(WeaponType)).Length);

        Weapons.Add(new EnemyWeapon(this, Game.Players[targetIndex], randomType));
        SwitchWeapon(0);

        Sprite.X = SplashKit.Rnd(Game.window.Width);
        Sprite.Y = 0;
        purseDistance = SplashKit.Rnd(250) + 150;
        SPEED = SplashKit.Rnd((int) SPEED) + 2;
    }

    public override void Draw() {
        base.Draw();
        Sprite.Draw();
        Weapon.Draw(Direction);
    }

    public override void Update() {
        if (targetIndex != -1) if (Game.Players[targetIndex].Health <= 0) targetIndex = -1;

        base.Update();

        if (Sprite.Dx != 0 && Sprite.AnimationHasEnded) Sprite.ReplayAnimation();

        Move();
    }

    private int GetNearestPlayerIndex() {
        int highestDist = 0, index = 0;

        for (int i = 0; i < Game.Players.Count; i++) {
            int dist = (int) SplashKit.PointPointDistance(Game.Players[i].Sprite.Position, Sprite.Position);
            if (dist > highestDist) {
                index = i;
                highestDist = dist;
            }
        }

        return index;
    }

    public void Move() {
        if (targetIndex == -1) {
            Sprite.Dx = 0;
            return;
        }

        Vector2D dir = SplashKit.VectorPointToPoint(Sprite.Position, Game.Players[GetNearestPlayerIndex()].Sprite.Position);

        //Face Target (X Axis)
        if (dir.X > 0) {
            Direction = Direction.RIGHT;
            if (!Sprite.AnimationName().Equals("ground_right")) Sprite.StartAnimation("ground_right");
        }
        else {
            Direction = Direction.LEFT;
            if (!Sprite.AnimationName().Equals("ground_left")) Sprite.StartAnimation("ground_left");
        }

        //Follow player if enemy is too far (X Axis)
        if (Math.Abs(dir.X) > purseDistance) {
            Sprite.Dx = dir.X > 0 ? SPEED / 2: -SPEED / 2;
            Sprite.Move();
        }
        else {
            Sprite.Dx = 0;
        }
    }

    public void Shoot() {
        if (targetIndex == -1) return;
        float target_y = Game.Players[targetIndex].Sprite.Y;
        if (Sprite.Y - Sprite.Height * 2 <= target_y && Sprite.Y + Sprite.Height * 2 > target_y) {
            Weapon.Shoot();
        }
    }
}