using System;
using System.Linq;
using System.Collections.Generic;
using SplashKitSDK;
using System.Threading;

public enum WindowState {
    MAIN_MENU, 
    MULTIPLAYER_MENU,
    SINGLE_PLAYER_GAME,
    MULTIPLAYER_GAME,
    MAP_PREVIEW
}

public class Program
{
    private static WindowState PrevState = WindowState.MAIN_MENU;
    public static WindowState State { get; private set; }
    public static void Main()
    {
        //Menu Window
        Window window = SplashKit.OpenWindow("Game", 500, 500);
        State = WindowState.MAIN_MENU;
        PlatformGame game = new PlatformGame(window);
        game.LoadAssests();
        game.GenerateMapFile();
        game.SetupEntites();
        GameWindow.SetupMenu(window);
        Thread thread = new Thread(game.HandleIncomingMessages);

        while (!SplashKit.QuitRequested()) {
            SplashKit.ProcessEvents();

            //Game -> Main Menu
            if ((PrevState == WindowState.SINGLE_PLAYER_GAME || PrevState == WindowState.MULTIPLAYER_GAME || PrevState == WindowState.MAP_PREVIEW) && State == WindowState.MAIN_MENU) {
                window.Resize(500, 500);
                GameWindow.SetupMenu(window);
                window.ToggleFullscreen();
                SplashKit.ShowMouse();
                PrevState = State;
            }  
            //Multiplayer Menu -> Main Menu
            else if (PrevState == WindowState.MULTIPLAYER_MENU && State == WindowState.MAIN_MENU){
                GameWindow.SetupMenu(window);
                PrevState = State;
            }
            //Main Menu -> Multiplayer Menu
            else if (PrevState == WindowState.MAIN_MENU && State == WindowState.MULTIPLAYER_MENU){
                GameWindow.SetupMultiplayerMenu(window);
                PrevState = State;
            }
            //Main or Multiplayer -> Game
            else if ((PrevState == WindowState.MAIN_MENU || PrevState == WindowState.MULTIPLAYER_MENU) && (State == WindowState.SINGLE_PLAYER_GAME || State == WindowState.MULTIPLAYER_GAME || State == WindowState.MAP_PREVIEW)) {
                window.Resize(1280, 720);
                window.ToggleFullscreen();
                SplashKit.HideMouse();
                SplashKit.MoveMouse(window.Width / 2, window.Height / 2); 

                if (PrevState == WindowState.MULTIPLAYER_MENU) game.Players[0].Name = GameWindow.InputFields.Where(x => x.name.Equals("username")).First().text;
                if (PrevState == WindowState.MAP_PREVIEW) game.SetupEntites();
                
                PrevState = State;
            }
            else if (State == WindowState.MAIN_MENU) {
                GameWindow.DrawButtons(window);
                GameWindow.HandleButtons();
            }
            else if (State == WindowState.MULTIPLAYER_MENU) {
                GameWindow.DrawMultiplayerMenu(window);
                GameWindow.HandleInputFields();
                GameWindow.HandleButtons();
            }
            else if (State == WindowState.SINGLE_PLAYER_GAME){
                game.SpawnEnemies();
                game.HandleInput();
                game.Update();
                game.DropItems();
                game.Draw();
            }
            else if (State == WindowState.MULTIPLAYER_GAME){
                if (!thread.IsAlive) thread.Start();
                game.Players[0].NetworkHandleInput();
                game.Update();
                game.Draw();
            }
            else if (State == WindowState.MAP_PREVIEW){
                game.HandleInput();
                game.Update();
                game.PreviewDraw();
            }            
        }

        GameServer.SendMessage(GameServer.GenerateJson(ServerMessageType.QUIT_GAME, game.Players[0], new String[]{"quit"}, new String[]{"true"}));
        GameServer.Stop();
        thread.Join();
    }

    public static void SetState(WindowState st) {
        PrevState = State;
        State = st;
        Console.WriteLine($"{PrevState} -> {State}");
    }

}
