using System;
using SplashKitSDK;

public enum WeaponType {
        RIFLE,
        PUMP_SHOTGUN,
        BAZOOKA,
        MINIGUN
};

public abstract class Weapon {
    public Bitmap image { get; protected set; }
    public WeaponType Type { get; protected set; }
    public float X { get; protected set; }
    public float Y { get; protected set; }
    protected Entity entity;
    public float Angle { get; protected set; }
    public float WeakAngle { get; protected set; }
    public int Ammo { get; protected set; }
    public int MaxAmmo { get; protected set; }
    public int Clips { get; protected set; }
    public int Speed { get; protected set; }
    public int Damage { get; protected set; }
    internal long prevTime;
    internal double fireRate;

    public Weapon(Entity entity, WeaponType type) {
        this.entity = entity;
        this.Type = type;
        WeakAngle = 0;
        Angle = 0;
        Clips = 0;

        if (Type == WeaponType.RIFLE) { Speed = 10; Damage = 2; Ammo = 50; MaxAmmo = 50; fireRate = 0.25; }
        else if (Type == WeaponType.PUMP_SHOTGUN) { Speed = 7; Damage = 5; Ammo = 7; MaxAmmo = 7; fireRate = 0.75; }
        else if (Type == WeaponType.BAZOOKA) { Speed = 3; Damage = 10; Ammo = 3; MaxAmmo = 3; fireRate = 1; }
        else if (Type == WeaponType.MINIGUN) { Speed = 15; Damage = 1; Ammo = 150; MaxAmmo = 150; fireRate = 0.05; }

        image = SplashKit.BitmapNamed($"weapon_{type.ToString().ToLower()}");
    }

    protected abstract float ComputeRealAngle();

    public void Attach() {
        if (!entity.Dummy) Angle = ComputeRealAngle();

        Y = entity.Sprite.Y + (entity.Sprite.Height / 2);

        if (entity.Direction == Direction.RIGHT) {
            X = entity.Sprite.X + (image.Width / 2);
        }
        else {
            X = entity.Sprite.X - (image.Width / 2);
        }
    }

    public void SetAngles(float a1, float a2) {
        Angle = a1;
        WeakAngle = a2;
    }

    public void Draw(Direction dir) {
        DrawingOptions opts = SplashKit.OptionRotateBmp(WeakAngle);  

        /*
         * FLIP GUN
         * Player Direction = RIGHT && Gun Angle = LEFT
         */
        if (dir == Direction.RIGHT && (Angle >= 90 && Angle <= 180 || Angle >= 180 && Angle <= 270)){
            opts.FlipX = true;
        }     
        /*
         * FLIP GUN
         * Player Direction = LEFT && Gun Angle = RIGHT
         */
        else if (dir == Direction.LEFT && (Angle >= 90 && Angle <= 180 || Angle >= 180 && Angle <= 270)){
            opts.FlipX = true;
        } 

        SplashKit.DrawBitmap(image, X, Y, opts);
    }

    public void Shoot() {
        long time = System.DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

        if ((double)(time - prevTime) / 1000f > fireRate) {
            if (HasAmmo()) {
                UseAmmo();
                entity.Game.Bullets.Add(new Bullet(entity.ID, this));
                SplashKit.PlaySoundEffect("shoot_gun");
            }

            prevTime = time;
        }
    }

    public void Reload() {
        if (Ammo == -1) return;

        if (HasClip()) {
            Ammo = MaxAmmo;
            Clips--;

            SplashKit.PlaySoundEffect("reload_gun");
        }
    }

    public void UseAmmo() {
        if (Ammo == -1) return;
        
        if (!HasAmmo() && HasClip()) {
            Clips--;
            Ammo--;
        }
        else if (HasAmmo()) {
            Ammo--;
        }
    }

    public bool HasAmmo() {
        if (Ammo == -1) return true;
        return Ammo > 0;
    }

    public bool HasClip() {
        return Clips > 0;
    }

    public bool AddClip() {
        Clips++;
        return true;
    }

    public void AddAmmo(int ammo) {
        if (Ammo + ammo > MaxAmmo) Ammo = MaxAmmo;
        else Ammo += ammo;
    }

    public static PlayerWeapon ConvertWeapon(Entity player, Weapon weapon) {
        PlayerWeapon p = new PlayerWeapon(player, weapon.Type);
        p.Ammo = weapon.Ammo;
        p.MaxAmmo = weapon.MaxAmmo;
        return p;
    }

    public void SetOwner(ref Entity ent) {
        entity = ent;
    }
}

public class PlayerWeapon : Weapon {
    public PlayerWeapon(Entity entity, WeaponType type) : base(entity, type) { }

    protected override float ComputeRealAngle() {
        WeakAngle = SplashKit.PointPointAngle(new Point2D{X = entity.Sprite.X, Y = entity.Sprite.Y}, PlatformGame.MousePosition());
        return WeakAngle < 0 ? 180 + (180 - Math.Abs(WeakAngle)): WeakAngle;
    }
}

public class EnemyWeapon : Weapon {
    private Player player;
    public EnemyWeapon(Entity entity, Player player, WeaponType type) : base(entity, type) { 
        this.player = player;
    }

    protected override float ComputeRealAngle() {
        if (player == null) return 0;

        WeakAngle = SplashKit.PointPointAngle(new Point2D{X = entity.Sprite.X, Y = entity.Sprite.Y}, player.Sprite.Position);
        return WeakAngle < 0 ? 180 + (180 - Math.Abs(WeakAngle)): WeakAngle;
    }

    public void SetTargetPlayer(Player player) {
        this.player = player;
    }
}

public class Bullet {
    public Sprite Sprite {
        get; private set;
    }

    public int Speed, Damage;
    public String ID { get; private set; }
    private Weapon weapon;

    public Bullet(String id, Weapon weapon) {
        Sprite = new Sprite(SplashKit.BitmapNamed("bullet_rifle"));  

        this.Speed = weapon.Speed;
        this.Damage = weapon.Damage;
        this.weapon = weapon;
        ID = id;

        Sprite.Rotation = weapon.Angle;
        //Position bullet along the gun body
        Sprite.X = ( weapon.X + (float) weapon.image.Width / (float) 2 ) - Sprite.Width / 2;
        Sprite.Y = ( weapon.Y + (float) weapon.image.Height / (float) 2 ) - Sprite.Height;
    }

    public bool IsOffScreen() {
        Window window = SplashKit.WindowNamed("Game");
        return Sprite.X < 0 || Sprite.X > window.Width || Sprite.Y < 0 || Sprite.Y > window.Height;
    }

    public void Draw() {
        Sprite.Draw();
    }

    public void Update() {
        Sprite.Dx = Speed;
        Sprite.Move();
        Sprite.Update();
    }
}