using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Linq;
using SplashKitSDK;
using System.Collections.Generic;

public class GameServer {
    public static string Ip { get; private set; }
    public static int Port { get; private set; }
    public static List<Socket> Clients { get; private set; }
    private static UdpClient socket;
    public static bool Enabled = false;
    private static IPEndPoint endPoint;

    public static void Setup(string ip, int port){
        Ip = ip;
        Port = port;
        socket = new UdpClient(Port);
        Clients = new List<Socket>();
        Enabled = true;
    }

    public static void StartServer() {
        //Dns.GetHostName()
        // IPHostEntry ipHost = Dns.GetHostEntry(Ip); 
        // IPAddress ipAddr = IPAddress.Any;
        // Ip = ipAddr.MapToIPv4().ToString();
        // IPEndPoint localEndPoint = new IPEndPoint(ipAddr, Port); 
        // socket.Bind(localEndPoint);
        // socket.Listen(10);
        Enabled = true;
    }

    public static void Connect() {
        //socket.Connect(Ip, Port);
    }

    public static void SendMessage(String msg) {
        // SplashKit.SetUDPPacketSize((uint) Encoding.UTF8.GetByteCount(msg));
        // socket.BroadcastMessage(msg);
        socket.Send(Encoding.UTF8.GetBytes(msg), Encoding.UTF8.GetByteCount(msg), endPoint);
    }

    public static String RecieveMessage() {
        // if (socket.HasNewConnections) socket.FetchNewConnection();

        // if (socket.HasMessages) {
        //     String data = socket.ReadMessage().Data;
        //     return data;
        // }

        // return String.Empty;
        endPoint = new IPEndPoint(IPAddress.Any, Port); 
        String data = Encoding.UTF8.GetString(socket.Receive(ref endPoint));
        return data;
    }

    public static void Stop() {
        Enabled = false;
        socket.Close();
    }
}