using System;
using SplashKitSDK;
using System.Net;

public class Program
{
    private static Json PrevData = new Json();

    public static void Main()
    {
        GameServer.Setup(Dns.GetHostName(), 80);

        GameServer.StartServer();

        Console.WriteLine($"Conneting to {GameServer.Ip}:{GameServer.Port}");

        while (GameServer.Enabled)
        {
            SplashKit.CheckNetworkActivity();

            String data = GameServer.RecieveMessage();;

            if (data == String.Empty) continue;

            Json json = SplashKit.JsonFromString(data);

            //Check For Dupe Location
            if (json.HasKey("x") && PrevData.HasKey("x")) {
                if (json.Equals(PrevData)) {
                    Console.WriteLine("Server >> Dupe messaged skipped!");
                    continue;
                }
            }

            PrevData = json;

            GameServer.SendMessage(data);
        }

        GameServer.Stop();
    }
}
